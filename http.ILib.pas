unit http.ILib;

interface

uses
  System.SysUtils,
  WinAPI.ActiveX;

type

  IHttp_plugin = interface
  ['{3A617FE4-80CA-49D9-A33D-DDBF0EF6431A}']
    function getProxy: WideString;
    procedure setProxy(const Value: WideString);

    function GetOnWorkEnd: TProc<IStream, UInt64>;
    procedure SetOnWorkEnd(const Value: TProc<IStream, UInt64>);

    procedure GET(aURL: WideString); overload;
    property Proxy: WideString read GetProxy write SetProxy;


    property OnWorkEnd: TProc<IStream, UInt64> read GetOnWorkEnd write SetOnWorkEnd;
  end;

implementation

end.
