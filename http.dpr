library http;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }


{$R *.res}

uses
  http.TLib in 'http.TLib.pas',
  http.ILib in 'http.ILib.pas';

function Http_plugin: IHttp_plugin;
begin
  Result:= THttp_plugin.Create;
end;

function Httpdyn_plugin: IHttp_plugin;
begin
  Result:= THttpdyn_plugin.Create;
end;

exports
  Http_plugin,
  Httpdyn_plugin;
end.
