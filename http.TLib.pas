unit http.TLib;

interface

uses
  System.SysUtils, System.Classes,
  Winapi.ActiveX,
  IdHttp, IdSSLOpenSSL, IdURI,
  http.ILib;

type

  THttp_plugin = class(TInterfacedObject, IHttp_plugin)
  private
    FIdHttp: TIdHTTP;
    FOnWorkEnd: TProc<IStream, UInt64>;
    fIdHandleSSL: TIdSSLIOHandlerSocketOpenSSL;
    function getProxy: WideString;
    procedure setProxy(const Value: WideString);
    function GetOnWorkEnd: TProc<IStream, UInt64>;
    procedure SetOnWorkEnd(const Value: TProc<IStream, UInt64>);
    procedure GET(aURL: WideString);
  public
    constructor Create;
    destructor Destroy; override;

    property Proxy: WideString read GetProxy write SetProxy;
  end;

  THttpThread = class(TThread)
  private
    FUrl: String;
    fProxyHost: String;
    fProxyPort: Integer;
    FOnResult: TProc<IStream, UInt64>;
    function GetProxy: WideString;
    procedure SetProxy(const Value: WideString);
  protected
    procedure Execute; override;
  public
    constructor Create;
    property Url: String read FUrl write FUrl;
    property Proxy: WideString read GetProxy write SetProxy;
    property OnResult: TProc<IStream, UInt64> read FOnResult write FOnResult;

  end;

  THttpdyn_plugin = class(TInterfacedObject, IHttp_plugin)
  private
    fProxyHost: String;
    fProxyPort: Integer;
    FOnWorkEnd: TProc<IStream, UInt64>;
    function getProxy: WideString;
    procedure setProxy(const Value: WideString);
    procedure GET(aURL: WideString);
    function GetOnWorkEnd: TProc<IStream, UInt64>;
    procedure SetOnWorkEnd(const Value: TProc<IStream, UInt64>);
  public
    constructor Create;
    destructor Destroy; override;
    property OnWorkEnd: TProc<IStream, UInt64> read GetOnWorkEnd write SetOnWorkEnd;
    property Proxy: WideString read GetProxy write SetProxy;
  end;

implementation

{ THttp_plugin }

constructor THttp_plugin.Create;
begin
  self.FIdHttp:= TIdHTTP.Create(nil);
  self.fIdHandleSSL:= TIdSSLIOHandlerSocketOpenSSL.Create(nil);
  self.FIdHttp.IOHandler:=self.fIdHandleSSL;
end;

destructor THttp_plugin.Destroy;
begin
  self.FIdHttp.Free;
  self.fIdHandleSSL.Free;
  inherited;
end;

procedure THttp_plugin.GET(aURL: WideString);
var
  _mstr:TMemoryStream;
  _istream: IStream;
  _url:TIdURI;
begin
  _mstr := TMemoryStream.Create;
  _url:=TIdURI.Create;
  try
    self.FIdHttp.Get(_url.URLEncode(aURL),_mstr);
    _mstr.Seek(0, TSeekOrigin.soBeginning);
    _istream:=TStreamAdapter.Create(_mstr, TStreamOwnership.soOwned);
    if Assigned(self.FOnWorkEnd) then self.FOnWorkEnd(_istream, _mstr.Size);
  finally
    _url.Free;
  end;
end;

function THttp_plugin.GetOnWorkEnd: TProc<IStream, UInt64>;
begin
  Result:=self.FOnWorkEnd;
end;

function THttp_plugin.getProxy: WideString;
begin
  Result:=self.FIdHttp.ProxyParams.ProxyServer+':'+IntToStr(self.FIdHttp.ProxyParams.ProxyPort);
end;

procedure THttp_plugin.SetOnWorkEnd(const Value: TProc<IStream, UInt64>);
begin
  self.FOnWorkEnd:=Value;
end;

procedure THttp_plugin.setProxy(const Value: WideString);
var
 _st: TArray<string>;
begin
  _st:= string(Value).Split([':']);
  if Length(_st)=0 then begin
    self.FIdHttp.ProxyParams.ProxyServer:= '';
    self.FIdHttp.ProxyParams.ProxyPort:= 0;
  end else begin
    self.FIdHttp.ProxyParams.ProxyServer:= _st[0];
    self.FIdHttp.ProxyParams.ProxyPort:= StrToInt(_st[1]);
  end;
end;

{ THttpThread_plugin }

constructor THttpdyn_plugin.Create;
begin
  self.fProxyHost:=EmptyWideStr;
  self.fProxyPort:=0;
end;

destructor THttpdyn_plugin.Destroy;
begin

  inherited;
end;

procedure THttpdyn_plugin.GET(aURL: WideString);
var
  _http_thread:THttpThread;
begin
  _http_thread:=THttpThread.Create;
  _http_thread.Proxy:=self.Proxy;
  _http_thread.Url:=aURL;
  _http_thread.OnResult:=self.FOnWorkEnd;
  _http_thread.Start;
end;

function THttpdyn_plugin.GetOnWorkEnd: TProc<IStream, UInt64>;
begin
  Result:=self.FOnWorkEnd;
end;

function THttpdyn_plugin.getProxy: WideString;
begin
  Result:=self.fProxyHost+':'+IntToStr(self.fProxyPort);
end;

procedure THttpdyn_plugin.SetOnWorkEnd(const Value: TProc<IStream, UInt64>);
begin
  Self.FOnWorkEnd:=Value;
end;

procedure THttpdyn_plugin.setProxy(const Value: WideString);
var
 _st: TArray<string>;
begin
  _st:= string(Value).Split([':']);
  if Length(_st)=0 then begin
    self.fProxyHost:= '';
    self.fProxyPort:= 0;
  end else begin
    self.fProxyHost:= _st[0];
    self.FProxyPort:= StrToInt(_st[1]);
  end;
end;

{ THttpThread }

constructor THttpThread.Create;
begin
  inherited Create(True);
  FreeOnTerminate:=True;
end;

procedure THttpThread.Execute;
var
  _mstr:TMemoryStream;
  _istream: IStream;
  _idhttp: TIdHTTP;
  _IdHandleSSL: TIdSSLIOHandlerSocketOpenSSL;
  _url:TIdURI;
begin
  _idhttp:=TIdHTTP.Create(nil);
  _IdHandleSSL:=TIdSSLIOHandlerSocketOpenSSL.Create(nil);
  _idhttp.ProxyParams.ProxyServer:=self.fProxyHost;
  _idhttp.ProxyParams.ProxyPort:=self.fProxyPort;
  _idhttp.IOHandler:=_IdHandleSSL;

  _url:=TIdURI.Create;
  _mstr := TMemoryStream.Create;
  _istream:=TStreamAdapter.Create(_mstr, TStreamOwnership.soOwned);
  try
    _idhttp.Get(_url.URLEncode(self.FUrl),_mstr);
    _idhttp.disconnect;
    _mstr.Position:=0;
    if Assigned(self.FOnResult) then self.FOnResult(_istream, _mstr.Size);
    _istream:=nil;
  finally
    _url.Free;
    _IdHandleSSL.Free;
    _idhttp.Free;
  end;
end;

function THttpThread.GetProxy: WideString;
begin
  Result:=self.fProxyHost+':'+IntToStr(self.fProxyPort);
end;

procedure THttpThread.SetProxy(const Value: WideString);
var
 _st: TArray<string>;
begin
  _st:= string(Value).Split([':']);
  if Length(_st)=0 then begin
    self.fProxyHost:= '';
    self.fProxyPort:= 0;
  end else begin
    self.fProxyHost:= _st[0];
    self.FProxyPort:= StrToInt(_st[1]);
  end;
end;

end.
