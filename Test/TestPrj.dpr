program TestPrj;

uses
  Vcl.Forms,
  MainForm in 'MainForm.pas' {fmMain},
  http.ILib in '..\http.ILib.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown:= True;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmMain, fmMain);
  Application.Run;
end.
