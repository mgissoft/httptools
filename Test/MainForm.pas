unit MainForm;

interface

uses
  {$IFDEF DEBUG} codesitelogging, {$ENDIF}
  Winapi.Windows, Winapi.Messages, WinApi.ActiveX,
  System.SysUtils, System.Variants, System.Classes, System.IOUtils, System.Math,
  System.Threading,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, http.ILib, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.Imaging.pngimage;

type
  TFFactroy = function: IHttp_plugin;

  TfmMain = class(TForm)
    edt_http: TEdit;
    btn_http: TSpeedButton;
    img1: TImage;
    edt_https: TEdit;
    btn_https: TSpeedButton;
    btnLoadTiles: TSpeedButton;
    pnl_buttons: TPanel;
    mmoLog: TMemo;
    procedure btn_httpClick(Sender: TObject);
    procedure btn_httpsClick(Sender: TObject);
    procedure btnLoadTilesClick(Sender: TObject);
  private
    FHttp: IHttp_plugin;
    FHttpdyn: IHttp_plugin;
  public
    procedure AfterConstruction; override;
  end;

var
  fmMain: TfmMain;

implementation

{$R *.dfm}

const
  cLibName = 'http.dll';
  cProcName = 'Http_plugin';
  cProcdynName = 'Httpdyn_plugin';

resourcestring
  rsErrorLoadLib        = '������ �������� ���������� %s';
  rsErrorLoadProcedure  = '������ �������� ��������� %s';

{ TfmMain }

procedure TfmMain.AfterConstruction;
var
  _h: THandle;
  _p: pointer;
  _ff: TFFactroy;
begin
  inherited;
  if self.FHttp = nil then
  begin
    _h := LoadLibrary(cLibName);
    if _h = 0 then
      raise Exception.CreateFmt(rsErrorLoadLIB, [cLibName]);
    _p := GetProcAddress(_h, cProcName);
    if _p = nil then
      raise Exception.CreateFmt(rsErrorLoadProcedure, [cProcName]);
    _ff := TFFactroy(_p);
    self.FHttp := _ff;
   _p := GetProcAddress(_h, cProcdynName);
    if _p = nil then
      raise Exception.CreateFmt(rsErrorLoadProcedure, [cProcdynName]);
    _ff := TFFactroy(_p);
    self.FHttpdyn:= _ff;
    self.FHttp.Proxy:='192.168.2.120:8080';
    self.FHttpdyn.Proxy:='192.168.2.120:8080';
  end;
end;


procedure TfmMain.btnLoadTilesClick(Sender: TObject);
var
  _x, _z, _i, _count: Integer;
  _base_path: String;
begin
  self.FHttpdyn.OnWorkEnd:=
  procedure(_iStream: IStream; _size: UInt64)
  var
    _mstream: TMemoryStream;
    _buffer:TBytes;
  begin
    SetLength(_buffer, _size);
    _istream.Read(_buffer, _size, nil);
    _mstream:=TMemoryStream.Create;
    _mstream.Write(_buffer, _size);
    _mstream.Seek(0, TSeekOrigin.soBeginning);
    _mstream.SaveToFile(TPath.Combine(_base_path, TPath.ChangeExtension(TPath.GetRandomFileName, '.png')));
    _mstream.Free;
  end;

  _base_path:=TPath.GetDirectoryName(Application.ExeName);
  for _z := 0 to 4 do begin
    _count:=Round(Power(2,_z));
    for _x := 0 to _count-1 do begin
      TParallel.For(0, _count-1, procedure(i: integer)
      var
        _url: String;
      begin
        _url:=String(edt_http.Text).Replace('{x}', IntToStr(_x)).Replace('{y}', IntToStr(i)).Replace('{z}', IntToStr(_z));
        _i:=i;
        TRY
          self.FHttpdyn.GET(_url);
        EXCEPT
          on E:Exception do begin
            {$IFDEF DEBUG} codesite.SendFmtMsg('���� z=%d, x=%d, y=%d',[_z, _x, i]); {$ENDIF}
            TThread.Synchronize(nil,
            procedure
            begin
              self.mmoLog.Lines.Add(Format('���� z=%d, x=%d, y=%d',[_z, _x, i]));
            end);
          end;
        END;
      end);
    end;
  end;
end;

procedure TfmMain.btn_httpClick(Sender: TObject);
var
  _url: String;
begin
  self.FHttp.OnWorkEnd:=
  procedure(_iStream: IStream; _size: UInt64)
  var
    _png:TPngImage;
    _mstream: TMemoryStream;
    _buffer:TBytes;
  begin
    SetLength(_buffer, _size);
    _istream.Read(_buffer, _size, nil);

    _mstream:=TMemoryStream.Create;
    _mstream.Write(_buffer, _size);
    _mstream.Seek(0, TSeekOrigin.soBeginning);

    _png:=TPngImage.Create;
    _png.LoadFromStream(_mstream);
    self.img1.Picture.Graphic:=_png;
    _png.Free;
    _mstream.Free;
  end;
  _url:=String(edt_http.Text).Replace('{x}', '0', [rfReplaceAll, rfIgnoreCase]).
    Replace('{y}', '0', [rfReplaceAll, rfIgnoreCase]).
    Replace('{z}', '0', [rfReplaceAll, rfIgnoreCase]);
  self.FHttp.GET(_url);
end;

procedure TfmMain.btn_httpsClick(Sender: TObject);
begin
  self.FHttp.OnWorkEnd:=
  procedure(_iStream: IStream; _size: UInt64)
  var
    _png:TPngImage;
    _mstream: TMemoryStream;
    _buffer:TBytes;
  begin
    SetLength(_buffer, _size);
    _istream.Read(_buffer, _size, nil);
    _mstream:=TMemoryStream.Create;
    _mstream.Write(_buffer, _size);
    _mstream.Seek(0, TSeekOrigin.soBeginning);
    _png:=TPngImage.Create;
    _png.LoadFromStream(_mstream);
    self.img1.Picture.Graphic:=_png;
    _png.Free;
    _mstream.Free;
  end;
  self.FHttp.GET(edt_https.Text);
end;

end.
